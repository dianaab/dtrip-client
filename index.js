// import React from 'react';
// import ReactDOM from 'react-dom';
// import Container from './src/components/Container';


// ReactDOM.render(
//   <Container />,
//   document.getElementById('root')
// );


import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import store from "./src/store/store.js";
import App from "./src/App.js";

// if you're in create-react-app import the files as:
// import store from "./js/store/index";
// import App from "./js/components/App.jsx";

render(
  <Provider store={store}>
    <App />
  </Provider>,
  // The target element might be either root or app,
  // depending on your development environment
  // document.getElementById("app")
  document.getElementById("root")
);

//PS: You need to create and export this "container component" in src to work ok?