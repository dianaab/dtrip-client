import React , { Component , useState}from 'react';
import { connect } from "react-redux";
import '../css/InputCustDataView.css';
import Select from "react-select";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import { getAllDestinationsData, selectStartDestination, selectEndDestination, updatePlanParameters } from "../actions/travelPlanActions";

export class InputCustDataView extends Component {

    constructor(props) {
        super(props);

        this.state = {
          startDestination:"",
          endDestination:"",
          numberFitsBaby:0,
          numberChildren47: 0,
          numberChildren812: 0,
          numberChildren1318: 0,
          numberSpecialNeeds:false,
          raiting :false,
          culture :false,
          romantic :false,
          museums :false,
          outdoors :false,
          beaches :false,
          shopping :false,
          relaxing :false,
          historicSites :false,
          wildlife :false,
          popular :false,
          balanced :false,
          hiddenGems :false,
          mustIncludes :false,
          startDate: new Date(),
          endDate: new Date()
         
        };
    
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        //this.handleChange = this.handleChange.bind(this);
        this.onOptionChange = this.onOptionChange.bind(this);
      }
    
      calculatePlan(){
        this.props.ccalculatePlan(this.state);
      }
      
      onOptionChange = (selectName,selectedOption) => {
        //do something with selectName and selectedOption
        let value = selectedOption;
        // this.setState({
        //   [selectName]: value
        // }, () => { console.log("state is set ->", this.state)});
            //console.log(this.state.startDestination);
         
         if(selectName ==="startDestination"){
          this.props.selectStartDestination(value);
         }else if (selectName ==="endDestination"){
          this.props.selectEndDestination(value);
         }
         this.setState({ [selectName]: value }, () => { console.log("state is set ->", this.state)})
      }

      handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
    
        this.setState(
          {[name]: value},
          () =>{this.props.updatePlanParameters(this.state)}
        );
        //this.props.updatePlanParameters(this.state);
      }

      // handleChange = (value, state) => {
      //   this.setState({ [state]: value });
      // };
      // handleChange = name => value => {
      //   this.setState({
      //     [name]: value
      //   });
      // }
        
      componentDidMount() {
        if(this.props.destinations==="" || this.props.destinations.length===0){
          this.props.getAllDestinationsData();
        } 
      }
      // handleChange = name => value => {
      //   if(name==="country"){
      //     if(!value || (value && value.length===0)){
      //       this.props.searchAttFilterClean();
      //     }else{
      //       this.props.selectCountry(value[0]);
      //     }
      //   }
      //   this.setState({
      //     [name]: value
      //   });
      //   };

        // handleChange (name, value) {
        //   if(name==="country"){
        //     if(!value || (value && value.length===0)){
        //       this.props.searchAttFilterClean();
        //     }else{
        //       this.props.selectCountry(value[0]);
        //     }
        //   }
        //   this.setState({
        //     [name]: value
        //   });
        //   };
      handleSubmit(event) {
        //alert('A name was submitted: ' + this.state.value);
        var parametersData={
            startDestination:event.target.elements.numberFitsBaby.startDestination,
            endDestination: event.target.elements.numberFitsBaby.endDestination,
            id : this.props.selectedLocation.id,
            numberFitsBaby: event.target.elements.numberFitsBaby.value,
            numberChildren47: event.target.elements.numberChildren47.value,
            numberChildren812: event.target.elements.numberChildren812.value,
            numberChildren1318: event.target.elements.numberChildren1318.value,
            numberSpecialNeeds: event.target.elements.numberSpecialNeeds.value,
            raiting :event.target.elements.raiting.value,
            culture :event.target.elements.culture.value,
            romantic :event.target.elements.romantic.value,
            museums :event.target.elements.museums.value,
            outdoors :event.target.elements.outdoors.value,
            beaches :event.target.elements.beaches.value,
            shopping :event.target.elements.shopping.value,
            relaxing :event.target.elements.relaxing.value,
            historicSites :event.target.elements.historicSites.value,
            wildlife :event.target.elements.wildlife.value,
            popular :event.target.elements.popular.value,
            balanced :event.target.elements.balanced.value,
            hiddenGems :event.target.elements.hiddenGems.value,
            mustIncludes :event.target.elements.mustIncludes.value
        };     
        this.props.updateLocation(parametersData);
        event.preventDefault();
      }
    
      // static getDerivedStateFromProps(props, state) {
      //   if(state.changedNow){
      //     return {
      //       changedNow:false
      //     };
      //   }

      //   if (props.selectedLocation &&props.selectedLocation.parametersData) {
      //     return {
      //       startDestination:props.selectedLocation.parametersData.startDestination,
      //       endDestination: props.selectedLocation.parametersData.endDestination,
      //       numberFitsBaby: props.selectedLocation.parametersData.numberFitsBaby,
      //       numberChildren47: props.selectedLocation.parametersData.numberChildren47,
      //       numberChildren812: props.selectedLocation.parametersData.numberChildren812,
      //       numberChildren1318: props.selectedLocation.parametersData.numberChildren1318,
      //       numberSpecialNeeds: props.selectedLocation.parametersData.numberSpecialNeeds,
      //       rating :props.selectedLocation.parametersData.raiting,
      //       culture :props.selectedLocation.parametersData.culture,
      //       romantic :props.selectedLocation.parametersData.romantic,
      //       museums :props.selectedLocation.parametersData.museums,
      //       outdoors :props.selectedLocation.parametersData.outdoors,
      //       beaches :props.selectedLocation.parametersData.beaches,
      //       shopping :props.selectedLocation.parametersData.shopping,
      //       relaxing :props.selectedLocation.parametersData.relaxing,
      //       historicSites :props.selectedLocation.parametersData.historicSites,
      //       wildlife :props.selectedLocation.parametersData.wildlife,
      //       popular :props.selectedLocation.parametersData.popular,
      //       balanced :props.selectedLocation.parametersData.balanced,
      //       hiddenGems :props.selectedLocation.parametersData.hiddenGems,
      //       mustIncludes :props.selectedLocation.parametersData.mustIncludes
      //     };
      //   }
      //   return {
      //    // startDestination: "",
      //     endDestination: "",
      //     numberFitsBaby: 0,
      //     numberChildren47: 0,
      //     numberChildren812: 0,
      //     numberChildren1318: 0,
      //     numberSpecialNeeds:0,
      //     rating :0,
      //     culture :false,
      //     romantic :false,
      //     museums :false,
      //     outdoors :false,
      //     beaches :false,
      //     shopping :false,
      //     relaxing :false,
      //     historicSites :false,
      //     wildlife :false,
      //     popular :false,
      //     balanced :false,
      //     hiddenGems :false,
      //     mustIncludes :false
      //   };
      // }

      render() {

        
        // const customStyles = {
        //   control: styles => ({ ...styles,   width: '40%' , color: 'grey'            

        //   }),
        //   option: styles => ({ ...styles,  width: '40%', color: 'grey'               

        //   }),
        //   menu: styles => ({ ...styles,                 
        //    width: '40%', color: 'grey'
        //   })                 

        // };
        console.log(this.state);
        const { startDestination, endDestination } = this.state;
        return (
          <form>
            {/*<div className ="viewName">Parameters</div>*/}

            <div className="startEndDestination">Start Destination</div>
            <div className = "selectCountry">
                  <Select
                    options={this.props.destinations}
                    value={startDestination}
                    autosize={true}
                    //onChange={this.handleChange("startDestination",this.value)}
                    onChange={e => this.onOptionChange("startDestination",e)}
                    //isMulti
                    placeholder="Select Start Destination"
                    searchable={ true }
                    clearable={ true } 
					        />
      			</div>
           <div className="startEndDestination">End Destination</div>
            <div className = "selectCountry">
                  <Select
                    options={this.props.destinations}
                    value={endDestination}
                    autosize={true}
                    onChange={e => this.onOptionChange("endDestination",e)}
                    //onChange={this.handleInputChange}
                    //isMulti
                    placeholder="Select End Destination"
                    searchable={ true }
                    clearable={ true } 
					        />
        </div>
        <div>
        <span className="dateBlock">  
          <div>
              <div className="startEndDestination">Start Date</div>
              <DatePicker
              selected={this.state.startDate}
              onChange={e => this.onOptionChange("startDate",e)}
              />
        </div>
        <div>
              <div className="startEndDestination">End Date</div>
              <DatePicker
              selected={this.state.endDate}
              onChange={e => this.onOptionChange("endDate",e)}
              />
        </div>
        </span>
        </div>
            <div className="form-group">
            <label>
                Baby (0-3):
              <input
                name="numberFitsBaby"
                type="number"
                class="locationElementView"
                value={this.state.numberFitsBaby}
                onChange={this.handleInputChange} />
            </label>
            <label>
              Children (4-7):
              <input
                name="numberChildren47"
                class="locationElementView"
                type="number"
                value={this.state.numberChildren47}
                onChange={this.handleInputChange} />
            </label>
            <label>
            Children (8-12):
              <input
                name="numberChildren812"
                class="locationElementView"
                type="number"
                value={this.state.numberChildren812}
                onChange={this.handleInputChange} />
            </label>  
            </div>
            <div className="form-group">
            <label>
            Children (13-18):
              <input
                name="numberChildren1318"
                class="locationElementView"
                type="number"
                value={this.state.numberChildren1318}
                onChange={this.handleInputChange} />
            </label>
            <label>
            Special needs:
              <input
                name="numberSpecialNeeds"
                class="locationElementView"
                type="checkbox"
                checked={this.state.numberSpecialNeeds}
                onChange={this.handleInputChange} />
            </label>
            {/*<label>
              Rating:
              <input
                name="rating"
                class="locationElementView"
                type="number"
                value={this.state.rating}
                onChange={this.handleInputChange} />
            </label>*/}
            </div>
            <div className="form-group">
            <label>
            Culture:
              <input
                name="culture"
                type="checkbox"
                class="locationElementView"
                checked={this.state.culture}
                onChange={this.handleInputChange} />
            </label>
            <label>
            Romantic:
              <input
                name="romantic"
                class="locationElementView"
                type="checkbox"
                checked={this.state.romantic}
                onChange={this.handleInputChange} />
            </label>
            <label>
            Museums:
              <input
                name="museums"
                class="locationElementView"
                type="checkbox"
                checked={this.state.museums}
                onChange={this.handleInputChange} />
            </label>
            </div>
            <div className="form-group">
            <label>
            Outdoors:
              <input
                name="outdoors"
                class="locationElementView"
                type="checkbox"
                checked={this.state.outdoors}
                onChange={this.handleInputChange} />
            </label>
            <label>
            Beaches:
              <input
                name="beaches"
                class="locationElementView"
                type="checkbox"
                checked={this.state.beaches}
                onChange={this.handleInputChange} />
            </label>
            <label>
            Shopping:
              <input
                name="shopping"
                class="locationElementView"
                type="checkbox"
                checked={this.state.shopping}
                onChange={this.handleInputChange} />
            </label>
            </div>
            <div className="form-group">
            <label>
            Relaxing:
              <input
                name="relaxing"
                class="locationElementView"
                type="checkbox"
                checked={this.state.relaxing}
                onChange={this.handleInputChange} />
            </label>
            <label>
            Historic sites:
              <input
                name="historicSites"
                class="locationElementView"
                type="checkbox"
                checked={this.state.historicSites}
                onChange={this.handleInputChange} />
            </label>
            <label>
            Wildlife:
              <input
                name="wildlife"
                class="locationElementView"
                type="checkbox"
                checked={this.state.wildlife}
                onChange={this.handleInputChange} />
            </label>
            </div>
            <div className="form-group">
            <label>
            Popular:
              <input
                name="popular"
                class="locationElementView"
                type="checkbox"
                checked={this.state.popular}
                onChange={this.handleInputChange} />
            </label>
            <label>
            Balanced:
              <input
                name="balanced"
                class="locationElementView"
                type="checkbox"
                checked={this.state.balanced}
                onChange={this.handleInputChange} />
            </label>
            <label>
            Hidden gems:
              <input
                name="hiddenGems"
                class="locationElementView"
                type="checkbox"
                checked={this.state.hiddenGems}
                onChange={this.handleInputChange} />
            </label>
            </div>
            {/*<div class="form-group">
            <label>
            Must includes:
              <input
                name="mustIncludes"
                class="locationElementView"
                type="number"
                value={this.state.mustIncludes}
                onChange={this.handleInputChange} />
            </label>
        </div>*/}
            {/*<div className ="submitButtom">
                <input type="submit" value="Submit" />
        </div>*/}
          </form>
        );
      }

}
function mapStateToProps(state) {
  return {
   //selectedAttraction: state.selectedAttraction,
   startDestination: state.planReducer.startDestination,
   endDestination: state.planReducer.endDestination,
   destinations: state.planReducer.destinations,
   //planParameters: state.planReducer.planParameters
  };
}
function mapDispatchToProps(dispatch) {
  return {
    getAllDestinationsData: () => dispatch(getAllDestinationsData()),
    selectStartDestination: startDestination => dispatch(selectStartDestination(startDestination)),
    selectEndDestination: endDestination => dispatch(selectEndDestination(endDestination)),
    updatePlanParameters: planParameters => dispatch(updatePlanParameters(planParameters))
   // updateAttraction: parametersData => dispatch(updateAttraction(parametersData)),
   // updateLocation: parametersData => dispatch(updateLocation(parametersData))
    //getAtttractionsData: () => dispatch(getAtttractionsData())
  };
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(InputCustDataView);