import React, { Component } from 'react';
import InputCustData from './InputCustData.js';
import ViewTravelPlan from './ViewTravelPlan.js';

const Container = () => {
    //return <h1>Hello World!</h1>
    return(
        <div>
            <InputCustData/>
            <ViewTravelPlan/>
        </div>
    );
    
}

export default Container