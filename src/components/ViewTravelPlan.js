import React, { Component } from 'react';
import MapComponent from './MapComponent1';
import "leaflet/dist/leaflet.css";

const ViewTravelPlan = () => {
    return (
        <div>
            <h1>View Travel Plan</h1>
            <div>
                <MapComponent/>
            </div>
            
        </div>
    )
}

export default ViewTravelPlan;