import React, { Component } from 'react'; 
import '../css/InputCustDataView.css';
import InputCustDataView from './InputCustDataView.js';
import { calculatePlan, calculatePlanTest } from "../actions/travelPlanActions";
import { connect } from "react-redux";

export class InputCustData extends Component {

    constructor(props) {
        super(props);
        this.calculatePlan = this.calculatePlan.bind(this);
    }

    calculatePlan(planParameters){
        this.props.calculatePlanTest();
    }
    render() {
//const InputCustData = () => {
    return (
        <div className="inputPatameterContaner">
            <h1>Input Parameters</h1>
            <button type="button" className="submitButtom" onClick={this.calculatePlan}>Generate Plan</button>
            <InputCustDataView/>
        </div>
        );
    }
}
function mapStateToProps(state) {
    return {
     //selectedAttraction: state.selectedAttraction,
     //startDestination: state.planReducer.startDestination,
    //  endDestination: state.planReducer.endDestination,
    //  destinations: state.planReducer.destinations
    planParameters: state.planReducer.planParameters
    };
  }
  function mapDispatchToProps(dispatch) {
    return {
        calculatePlanTest: () => dispatch(calculatePlanTest()),
        calculatePlan: planParameters => dispatch(calculatePlan(planParameters)),
    };
  }
  export default connect(
      mapStateToProps,
      mapDispatchToProps
    )(InputCustData);