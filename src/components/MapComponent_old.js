import React , { Component }from 'react';
import { connect } from "react-redux";

import { Map, Marker, Popup, TileLayer } from "react-leaflet";
//import { Icon } from "leaflet";
import * as parkData from "../data/skateboard-parks.json";
import "../css/MapComponent.css";

//https://blog.logrocket.com/how-to-use-react-leaflet/
//export default function MapComponent() {
export class MapComponent extends Component  {

  render() {
    const [activePark, setActivePark] = React.useState(null);
  return (
    <Map center={[45.4, -75.7]} zoom={12}>
    <TileLayer
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
      />

    {parkData.features.map(park => (
      <Marker
        key={park.properties.PARK_ID}
        position={[
          park.geometry.coordinates[1],
          park.geometry.coordinates[0]
        ]}
        onClick={() => {
          setActivePark(park);
        }}
      />
    ))}

     {activePark && (
    <Popup
      position={[
        activePark.geometry.coordinates[1],
        activePark.geometry.coordinates[0]
      ]}
      onClose={() => {
        setActivePark(null);
      }}
    >
      <div>
        <h2>Test</h2>
        <p>{activePark.properties.DESCRIPTIO}</p>
      </div>
    </Popup>
  )}
  </Map>
  );
    }
}
function mapStateToProps(state) {
  return {
   //selectedAttraction: state.selectedAttraction,
   calculatedPlan: state.planReducer.calculatedPlan,
  //  endDestination: state.planReducer.endDestination,
  //  destinations: state.planReducer.destinations
  };
}
function mapDispatchToProps(dispatch) {
  return {
    // calculatePlanTest: () => dispatch(calculatePlanTest()),
    // selectStartLocation: locationData => dispatch(selectStartLocation(locationData)),
    // selectEndLocation: locationData => dispatch(selectEndLocation(locationData))
  };
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(MapComponent);
