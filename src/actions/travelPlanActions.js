import { 
    SELECT_END_DESTINATION,
    SELECT_START_DESTINATION,
    RETRIVE_ALL_DESTINATIONS,
    NOTHING_TO_DO,
    SEARCH_ATT_FILTER_CLEAN,
    CALCULATE_PLAN,
    UPDATE_PLAN_PARAMETERS
  } from "../constants/action-types";
  
  const baseUrl = "http://localhost:5000";

  //get all Locations 
export function getAllDestinationsData() {
    return function(dispatch) {
      // if(!country || country.length===0){
      //   return dispatch({ type: NOTHING_TO_DO});
      // }else{
      return fetch(baseUrl +"/getAllDestinations")
        .then(response => response.json())
        .then(json => {
          dispatch({ type: RETRIVE_ALL_DESTINATIONS, payload: json });
        });
    // };
   }
  }
  export function selectStartDestination(payload) {
    return { type: SELECT_START_DESTINATION, payload };
  }
  export function selectEndDestination(payload) {
    return { type: SELECT_END_DESTINATION, payload };
  }
  export function calculatePlanTest() {
    return function(dispatch) {
      // if(!country || country.length===0){
      //   return dispatch({ type: NOTHING_TO_DO});
      // }else{
      return fetch(baseUrl +"/calculatePlanJson")
        .then(response => response.json())
        .then(json => {
          dispatch({ type: CALCULATE_PLAN, payload: json });
        });
    // };
   }
  }
  
  export function calculatePlan(payload) {
    return function(dispatch) {
    //   if(!country){
    //     return dispatch({ type: NOTHING_TO_DO});
    //   }else{
      return fetch(baseUrl +"/calculatePlan", {
              method: "POST",
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
              body: JSON.stringify(payload)         
            })
       .catch(err => {
          console.log(err);
          alert("Calculate Plan failed, please try again!");
        })
        .then(response => response.json())
        .then(json => {
          dispatch({ type: CALCULATE_PLAN, payload: json });
       });
     //}
   }
  }

  export function updatePlanParameters(payload) {
    return { type: UPDATE_PLAN_PARAMETERS, payload };
  }