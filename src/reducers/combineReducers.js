import { combineReducers } from 'redux'
import planReducer from './planReducer';
import modalReducer from './modalReducer';

export default combineReducers({
    planReducer,
    modalReducer
})