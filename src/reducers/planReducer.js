import { 
    SELECT_END_DESTINATION,
    SELECT_START_DESTINATION,
    RETRIVE_ALL_DESTINATIONS,
    NOTHING_TO_DO,
    SEARCH_ATT_FILTER_CLEAN,
    CALCULATE_PLAN,
    UPDATE_PLAN_PARAMETERS
   } from "../constants/action-types";
  
  const initialState = {
    destinations: [],
    startDistination: null,
    endDistination: null,
    calculatedPlan: null,
    planParameters: null
  };
  
  function planReducer(state = initialState, action) {
//     if (action.type === ADD_ARTICLE) {
//       return Object.assign({}, state, {
//         articles: state.articles.concat(action.payload)
//       });
//     }
  
//     if (action.type === "DATA_LOADED") {
//       return Object.assign({}, state, {
//         remoteArticles: state.remoteArticles.concat(action.payload)
//       });
//     }
  
//     if (action.type === "ATTRACTION_DATA_LOADED" || action.type === ADD_ATTRACTION) {
//       return Object.assign({}, state, {
//         attractions: state.attractions.concat(action.payload)
//       });
//     }
  
//     if (action.type === SELECT_ATTRACTION) {
//       return Object.assign({}, state, {
//         selectedAttraction: action.payload
//       });
//     }
    
//     if (action.type === SELECT_COUNTRY) {
//       return Object.assign({}, state, {
//         selectedCountry: action.payload
//       });
//     }
//     if (action.type === UPDATE_ATTRACTION) {
//       return Object.assign({}, state, {
//         attractions: state.attractions.map(attraction => {
//           if(attraction.id === action.payload.id){
//             attraction.parametersData = action.payload.result.parametersData;
//           };
//           return attraction;
//        })
//       });
//     }
    
    //Location
    if (action.type === RETRIVE_ALL_DESTINATIONS) {
      return Object.assign({}, state, {
        destinations: state.destinations.concat(action.payload.map((dist,index)=>{
            let stringValue;
            if(dist.city && dist.city !=""){
              stringValue= dist.city+", "+ dist.country;
             }else{
              stringValue= dist.country; 
             }
            let newdist={
                key: index, 
                label:stringValue,
                value:stringValue
            };
            return newdist;
        }))
      });
    }
  
    if (action.type === SELECT_END_DESTINATION) {
      return Object.assign({}, state, {
        endDistination: action.payload
      });
    }
    if (action.type === SELECT_START_DESTINATION) {
        return Object.assign({}, state, {
            startDistination: action.payload
        });
      }
    if (action.type === CALCULATE_PLAN) {
        return Object.assign({}, state, {
            calculatedPlan: action.payload
        });
    }
    if (action.type === UPDATE_PLAN_PARAMETERS){
      return Object.assign({}, state, {
        planParameters  : action.payload
      });
  }
//     if (action.type === UPDATE_LOCATION) {
//       return Object.assign({}, state, {
//         locations: state.locations.map(location => {
//           if(location.id === action.payload.id){
//             location.parametersData = action.payload.result.parametersData;
//           };
//           return location;
//        })
//       });
//     }
  
//     if (action.type === RETRIVE_SUPPORTE_COUNTRIES) {
//       return Object.assign({}, state, {
//         supportedCountries: state.supportedCountries.concat(action.payload)
//       });
//     }
  
//     if (action.type === NOTHING_TO_DO){
//       return state;
//     }
  
//     if (action.type === SEARCH_ATT_FILTER_CLEAN){
//       return Object.assign({}, state, {
//         attractions: [],
//         selectedAttraction: null
//       });
//     }
  
//     if (action.type === DELETE_ATTRACTION){
//       return Object.assign({}, state, {
//         attractions: state.attractions.filter(attraction => {return attraction.id !== action.payload.id }),
//         selectedAttraction: null
//       });
//     }
     return state;
   }

export default planReducer;