import React from 'react';
// import {Switch, Route, BrowserRouter as Router} from 'react-router-dom';
// import AttractionContainer from './subcomponent/AttractionContainer';
// import LocationsContainer from './subcomponent/LocationsContainer';
// import AddAttractionContainer from './subcomponent/AddAttractionContainer';
// import MainContaner from './MainContaner';

// export const appRoutesConfig =[
//     {path: '/', component: MainContaner},
//     {path: '/attractions', component: AttractionContainer},
//     {path: '/locations', component: LocationsContainer},
//     {path: '/addAttraction', component: AddAttractionContainer}
// ];

// export const createRoute = (path, component, props) =>(
//     <Route exact path={path} key={path} component={component} {...props} />
// );

// export const getAppRoutes = (routes) => {
//     return routes.map(route => createRoute(route.path,route.component,route.props));
// }

// export const buildRouter = (routesConfig, mapRoutes = getAppRoutes) =>{
//     const appRoutes = mapRoutes(routesConfig);
//     return(
//         <Router>
//             <Switch>
//                 {appRoutes}
//             </Switch>
//         </Router>
//     );
// }

// export default () => buildRouter(appRoutesConfig, getAppRoutes);